﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SolrNetDemo.Business;
using SolrNetDemo.Models;
using Swashbuckle.Swagger.Annotations;
namespace SolrNetDemo.Controllers
{
    /// <summary>
    /// Add and update documents to the Solr index and execute queries.
    /// </summary>
    public class SolrIndexController : ApiController
    {
        private readonly Indexer _indexer = new Indexer();
        private readonly StreamIndexer _streamIndexer = new StreamIndexer();

        private readonly Searcher _searcher = new Searcher();
        /// <summary>
        /// Query(string searchTerms) - Execute a Solr query against the index.
        /// </summary>
        /// <param name="searchTerms">The search terms as a phrase.</param>
        /// <returns>The search result</returns>
        [SwaggerResponse(HttpStatusCode.OK, "Success", typeof(IList<SolrDoc>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "No documents found", typeof(IList<SolrDoc>))]
        public HttpResponseMessage Get(string searchTerms = null)
        {
            var result = _searcher.Query(searchTerms);
            if (!result.Any())
                return Request.CreateResponse(HttpStatusCode.NoContent);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// ClearIndex() - Deletes the entire Solr index.
        /// </summary>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, "Index is deleted", typeof(string))]
        public HttpResponseMessage Delete()
        {
            _streamIndexer.ClearIndex();
            return Request.CreateResponse(HttpStatusCode.OK, "Solr index is cleared and now completely empty.");
        }

        /// <summary>
        /// Index(string title, string text) - Add a document with title and text to the Solr index.
        /// </summary>
        /// <param name="title">The title of the document to put in the index.</param>
        /// <param name="text">The text content of the document to put in the index</param>
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(string))]
        [SwaggerResponse(HttpStatusCode.Created, "Document created succesfully", typeof(string))]
        public HttpResponseMessage Put(string title, string text = null)
        {
            _indexer.Index(title, text);
            return Request.CreateResponse(HttpStatusCode.Created, string.Concat("Succuesfully indexed ", title));
        }

        /// <summary>
        /// IndexFiles(string folderPath) - Add all files in folder to the index using ExtractingRequestHandler.
        /// </summary>
        /// <param name="folderPath">Path to a list of folders Available formats are 'pdf', 'word', 'ppt' or 'html'.</param>
        /// <returns></returns>
        /// <response code="400">Bad request. The command isnt supported.</response>
        /// <response code="500">Server error. Check the log.</response>
        public string Post(string folderPath)
        {
            return string.Format("Indexed {0} file(s) found in {1}", _streamIndexer.IndexFiles(folderPath), folderPath);
        }
    }
}
