﻿using SolrNet.Attributes;

namespace SolrNetDemo.Models
{
    public class SolrDoc
    {
        [SolrUniqueKey("id")]
        public string Id { get; set; }

        [SolrField("title")]
        public string Title { get; set; }

        [SolrField("text")]
        public string Text { get; set; }
    }
}