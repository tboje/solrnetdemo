﻿using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNetDemo.Models;

namespace SolrNetDemo.Business
{
    public class Searcher
    {
        private ISolrOperations<SolrDoc> _solr = ServiceLocator.Current.GetInstance<ISolrOperations<SolrDoc>>();

        public IList<SolrDoc> Query(string searchTerms)
        {
            IList<SolrDoc> result = new List<SolrDoc>();

            var titleQuery = new SolrQueryByField("title",searchTerms);
            var textQuery = new SolrQueryByField("text", searchTerms);

            var options = GetProximityOptions();
            result = _solr.Query(titleQuery || textQuery, options);
            
            return result;
        }

        public QueryOptions GetProximityOptions()
        {
            var localParams = new LocalParams
            {
                {"defType","edismax"}, //Extended DisMax Query parser
                {"qs","4"}, // qs = Query Slop
            };

            var options = new QueryOptions() { ExtraParams = localParams };
            return options;
        }
    }
}