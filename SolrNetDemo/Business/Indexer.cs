﻿using System;
using Microsoft.Practices.ServiceLocation;
using SolrNet;
using SolrNetDemo.Models;

namespace SolrNetDemo.Business
{
    public class Indexer
    {
        private ISolrOperations<SolrDoc> _solr = ServiceLocator.Current.GetInstance<ISolrOperations<SolrDoc>>();

        public void Index(string title, string text)
        {
            var doc = new SolrDoc();
            doc.Id = Guid.NewGuid().ToString();
            doc.Title = title;
            doc.Text = text;

            //solr/extractdemo/update
            _solr.Add(doc);
            _solr.Commit();
        }
    }
}