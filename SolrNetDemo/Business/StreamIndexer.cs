﻿using System.IO;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using SolrNet;
using SolrNetDemo.Models;

namespace SolrNetDemo.Business
{
    public class StreamIndexer
    {
        private ISolrOperations<SolrDoc> _solr = ServiceLocator.Current.GetInstance<ISolrOperations<SolrDoc>>();

        public void IndexFile(string filePath)
        {
            using (var stream = File.OpenRead(filePath))
            {
                var extractParameters = new ExtractParameters(stream, Path.GetFileName(filePath));
                _solr.Extract(extractParameters);
            }
        }

        public int IndexFiles(string folderPath)
        {
            var filesToIndex = Directory.GetFiles(folderPath);
            foreach (var filePath in filesToIndex)
            {
                IndexFile(filePath);
            }

            _solr.Commit();

            return filesToIndex.Count();
        }
        
        //public int IndexHtml(string url)
        //{
        //    var bytes = Download(url);
        //    using (Stream stream = new MemoryStream(bytes))
        //    {
        //        var extractParameters = new ExtractParameters(stream, url, url);
        //        extractParameters.ExtractFormat = ExtractFormat.XML;
        //        extractParameters.ExtractOnly = false;
        //        _solr.Extract(extractParameters);
        //    }
        //    _solr.Commit();

        //    return 1;
        //}

        //private byte[] Download(string url)
        //{
        //    using (var client = new WebClient())
        //    {
        //        var doc = client.DownloadData(url);
        //        return doc;
        //    }
        //}

        public void ClearIndex()
        {
            var deleteIndexQuery = new SolrQuery("*:*");
            _solr.Delete(deleteIndexQuery);
            _solr.Commit();
        }
    }
}